package com.silvioapps.fakewallet

import android.os.Bundle
import android.text.SpannableStringBuilder
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.bold

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val s = SpannableStringBuilder()
            .append(getString(R.string.warning_1))
            .bold {append(getString(R.string.warning_2))}
            .append(getString(R.string.warning_3))
        val tv = findViewById<TextView>(R.id.warning)
        tv.text = s
    }
}